const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const { v4: uuid } = require("uuid");

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();

  api.use(express.json());
  api.use(cors(corsOptions));


  // Hello World :D
  api.get("/", (req, res) => res.json("Hello, World!"));
  
  // Accessing mongo methods
  db = mongoClient.db().collection()

  //Adding a posted user on MongoDB (if it pass on "testNewUser()")
  api.post("/users", testNewUser, async (req, res) => {
    
    UUID = uuid()

    //UserCreated JSON message sent to broker
    const natsUserCreated = 
    { "eventType": "UserCreated",
      "entityId": UUID,
      "entityAggregate": {
          "name": req.body.name,
          "email": req.body.email,
          "password": req.body.password,
        }
    }

    //UserCreated JSON added to MongoDB
    const UserCreated = 
    { "entityId": UUID,
      "entityAggregate": {
          "name": req.body.name,
          "email": req.body.email,
          "password": req.body.password,
        }
    }

    //Publishing UserCreated Message
    
    
    //If it doesn't find the user posted, post publish the new user
    if(!await db.findOne({"id": UserCreated.entityId}))
    {
      //Publishes the new user
      stanConn.publish('users', natsUserCreated)
    }
    

  })

  //Deleting an user after a delete request (if it pass on "testAccessToken()")
  api.delete("/users/:uuid", testAccessToken, async (req, res) => {
    const id = req.params.uuid;

    //UserDeleted JSON message sent to broker
    const natsUserDeleted = 
    { "eventType": "UserDeleted",
      "entityId": id,
      "entityAggregate": {}      
    }

    //If it finds the requested user on mongo, publish the deletion
    if(await db.findOne({id: id}))
    {
      stanConn.publish('users', natsUserDeleted)
    }
  })


  // Test if new user data is correct
  function testNewUser(req, res, next)
  {
    var flag = 1
    //Verifying if there is any empty fields
    const nuKeys = Object.keys(req.body);
    for (let i = 0; i < nuKeys.length; i++){
      if(req.body[nuKeys[i]].length <= 0)
      {
        console.log({"error": `Request body had missing field {${nuKeys[i]}}`});
        res.status(400).json({"error": `Request body had missing field {${nuKeys[i]}}`}) 
        flag = 0;       
      }
    }

    //Verifying if email is worth as STRING@STRING.STRING
    const reemail = /\S+@\S+\.\S+/;
    if(!reemail.test(req.body.email))
    {
      console.log({"error": "Request body had malformed field {email}"});
      res.status(400).json({"error": "Request body had malformed field {email}"});
      flag = 0;
    }


    //Verifying if password is alphanumeric and within size = [8,32]
    const repass = /^[a-z0-9]+$/i;
    const passSize = req.body.password.length
    if(!repass.test(req.body.password) ||  passSize < 8 || passSize > 32)
    {
      console.log({"error": "Request body had malformed field {password}"});
      res.status(400).json({"error": "Request body had malformed field {password}"});
      flag = 0;
    }

    //Verifying if password === passwordConfirmation
    if(req.body.password !== req.body.passwordConfirmation)
    {
      console.log({"error": "Password confirmation did not match"});
      res.status(422).json({"error": "Password confirmation did not match"});
      flag = 0;
    }

    //Everythings is great! :D
    if(flag === 1)
    {
      const id = uuid();
      const goodmsg =       
      {"user": 
        {"id": id, 
        "name": req.body.name, 
        "email": req.body.email
        }
      }
      res.status(201).json(goodmsg);
      next();
    }
  }

  // Testing if access token is valid
  function testAccessToken(req, res, next)
  {
    var flagg = 1
    var zztoken 
    
    // Verifying if the header is ok
    if( !(zztoken = req.get("Authentication").split(" "))
        ||zztoken[0] !== "Bearer" || zztoken[1] === '')
    { 
      console.log({"error": "Access Token not found"});
      res.status(401).json({"error": "Access Token not found"});
      flagg = 0
    }

    //getting token
    const token = zztoken[1];
    const newId = jwt.decode(token).id
    const id = req.params.uuid;

    //testing if decode token === id
    if(id !== newId)
    {
      console.log({"error": "Access Token did not match User ID"});
      res.status(403).json({"error": "Access Token did not match User ID"});
      flagg = 0
    }

    //Everythings is great! :D
    if(flagg === 1)
    {
      const successDel =
      {
        "id": id
      }
      res.status(200).json(successDel);
      next()
    }
    
  }

  return api;
}
